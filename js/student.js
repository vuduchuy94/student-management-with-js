// Validate email
function emailIsValid(email){
  return  /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}
function save(){
// Get Value
  let fullname = document.getElementById('fullname').value; 
  let email = document.getElementById('email').value; 
  let phone = document.getElementById('phone').value; 
  let address = document.getElementById('address').value; 
  let gender = '';
  if(document.getElementById('male').checked){
    gender=document.getElementById('male').value;
  } 
  else if(document.getElementById('female').checked){
    gender=document.getElementById('female').value;
  }

  // validate form: name
if(_.isEmpty(fullname)){
    fullname = '';
    document.getElementById('fullname-error').innerHTML='氏名が必要です';
}
else if(fullname.trim().length <= 2) {
    fullname = '';
    document.getElementById('fullname-error').innerHTML='2文字以上にしてください';
}
else if(fullname.trim().length > 40) {
    fullname = '';
    document.getElementById('fullname-error').innerHTML='40文字以下にしてください';
}
else{
    document.getElementById('fullname-error').innerHTML='';
}
 // validate form: email
if(_.isEmpty(email)){
    email = '';
    document.getElementById('email-error').innerHTML='メールが必要です';
}
else if(!emailIsValid(email)) {
    email = '';
    document.getElementById('email-error').innerHTML='もう一度入力してください';
}
else{
    document.getElementById('email-error').innerHTML='';
}
 // validate form: phone
 if(_.isEmpty(phone)){
    phone = '';
    document.getElementById('phone-error').innerHTML='携帯電話が必要です';
}
else{
    document.getElementById('phone-error').innerHTML='';
}
 // validate form: address
 if(_.isEmpty(address)){
    address = '';
    document.getElementById('address-error').innerHTML='アドレスが必要です';
}
else{
    document.getElementById('address-error').innerHTML='';
}
// validate form: gender
if(_.isEmpty(gender)){
    gender = '';
    document.getElementById('gender-error').innerHTML='性別が必要です';
}
else{
    document.getElementById('gender-error').innerHTML='';
}
//学生一覧フォームに保存する
if(fullname && email && phone && address && gender){
    let students = localStorage.getItem('students')? JSON.parse(localStorage.getItem('students')): [];
    students.push({
        fullname: fullname,
        email: email,
        phone: phone,
        address: address,
        gender: gender
    });
    localStorage.setItem('students', JSON.stringify(students));
    this.renderListStudent();
}
}
function renderListStudent(){
    let students = localStorage.getItem('students')? JSON.parse(localStorage.getItem('students')): [];
    if(students.length === 0) {
        document.getElementById('list-student').style.display='none' ;
        return false; 
    }  else
    document.getElementById('list-student').style.display='block' ;
    let tableContent = `<tr>
    <td>#</td>
    <td>氏名</td>
    <td>メールアドレス</td>
    <td>携帯電話</td>
    <td>アドレス</td>
    <td>性別</td>
    <td>行動</td>
</tr>`;
students.forEach((student, index)=> {
    
    let studentId = index;
    let genderLable =parseInt(student.gender) === 1 ? '男':'女';
    index++;
    tableContent += `<tr>
    <td>${index}</td>
    <td>${student.fullname}</td>
    <td>${student.email}</td>
    <td>${student.phone}</td>
    <td>${student.address}</td>
    <td>${genderLable}</td>
    <td>
        <a href='#'>Edit</a> | <a href='#' onclick='deleteStudent(${studentId})'>Delete</a>
    </td>
</tr>`;
})
document.getElementById('grid-students').innerHTML = tableContent;
}
// Delete student
function deleteStudent(studentId){
    let students = localStorage.getItem('students')? JSON.parse(localStorage.getItem('students')): [];
    students.splice(studentId,1);
    localStorage.setItem('students', JSON.stringify(students));
    renderListStudent();
}